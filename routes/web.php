<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RefController;

Route::get('/produk-js',function(){
    return view('demojs.product');
});


Route::get('/senarai-produk', [ProductController::class,'senaraiProduk'])->name('product.list');
Route::get('/carian-produk', [ProductController::class,'senaraiProduk']);
Route::get('/tambah-produk', [ProductController::class,'borangProduk'])->name('product.add');
Route::get('/kemaskini-produk/{id}', [ProductController::class,'borangProduk']);
Route::post('/simpan-produk/{id?}', [ProductController::class,'simpanProduk']);
Route::delete('/hapus-produk/{id}', [ProductController::class,'hapusProduk']);

Route::Resource('ref', RefController::class);

Route::get('/', function () {
    return view('layouts.admin');
});
