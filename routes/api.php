<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/senarai-produk-js', [ProductController::class,'senaraiProdukJs'])->name('product.list.js');
Route::delete('/hapus-produk-js/{id}', [ProductController::class,'hapusProdukJs'])->name('product.delete.js');
Route::get('/carian-produk-js', [ProductController::class,'senaraiProdukJs']);
Route::get('/tambah-produk-js', [ProductController::class,'borangProdukJs'])->name('product.add.js');
Route::get('/kemaskini-produk-js/{id}', [ProductController::class,'borangProdukJs']);
Route::post('/simpan-produk-js/{id?}', [ProductController::class,'simpanProdukJs']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
