@extends('layouts.admin')
@section('title', 'Senarai Produk')
@section('content')
<h1>Senarai Produk</h1>
<br>
<form action="/carian-produk">
    Nama Produk <input type="text" name="product_name" value="{{$product_name}}">
    <br>
    Jenis Produk 
    <select name="product_cat">
        <option value="">-- Sila Pilih --</option>
        @foreach($categories as $item)
        <option value="{{$item->kod}}" {{($item->kod == $product_cat ? 'selected':'')}}>
            {{$item->penerangan}}
        </option>
        @endforeach
    </select>
    <br>
    <button type="submit" class="btn btn-primary">Teruskan</button>
    <a href="/senarai-produk"><button type="button" class="btn btn-danger">Set Semula</button></a>
</form>
<br>
<br>
<a href="/tambah-produk"><button class="btn btn-info">Tambah</button></a>
<table class="table table-striped">
    <tr>
        <th>Bil</th>
        <th>Nama Produk</th>
        <th>Jenis Produk</th>
        <th>Tindakan</th>
    </tr>
    @php 
    $i = $mprod->firstItem();
    @endphp
    @foreach($mprod as $data)

        <tr>
            <td>{{$i++}}</td>
            <td>{{$data->product_name}}</td>
            <td>{{$data->getCategory->penerangan}}</td>
            <td>
            
            <a href="/kemaskini-produk/{{$data->id}}"><button class="btn btn-success">Kemaskini</button></a>  
            <form action="/hapus-produk/{{$data->id}}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" onclick="return confirm('Anda pasti?')" class="btn btn-danger">Hapus</button>
            </form>            
            </td>
        </tr>

    @endforeach
    

</table>
{{ $mprod->links() }}
@endsection