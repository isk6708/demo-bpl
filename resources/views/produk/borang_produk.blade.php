@extends('layouts.admin')
@section('title', 'Borang Produk')
@section('content')
<h1>Borang Produk</h1>

@if ($errors->any())
    <br>
    <div style="color:red" class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(!empty($mprod->id))
<form action="/simpan-produk/{{$mprod->id}}" method="POST" class="row mb-12">
@else
<form action="/simpan-produk" method="POST" class="row mb-12">
@endif

    @csrf    
     
    <div class="mb-3 row">
        <label class="form-label">Nama Produk</label>
        <input type="text" name="product_name" value="{{$mprod->product_name}}" class="form-control">
    </div>
    <div class="row">
        <label class="form-label">Jenis Produk </label>
        <select name="product_cat" class="form-control">
            <option value="">-- Sila Pilih --</option>
            @foreach($categories as $item)
            <option value="{{$item->kod}}" {{($item->kod == $mprod->product_cat ? 'selected':'')}}>
                {{$item->penerangan}}
            </option>
            @endforeach
        </select>
    </div>
    <!-- <div class="row"> -->
        <a href="#"><button class="btn btn-primary">Simpan</button></a>
        <a href="/senarai-produk"><button class="btn btn-danger" type="button">Kembali</button></a>
    <!-- </div>     -->
</form>
@endsection