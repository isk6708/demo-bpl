<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Ref;

class Product extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function getCategory(){
        return $this->hasOne(Ref::class,'kod','product_cat');
    }
}
