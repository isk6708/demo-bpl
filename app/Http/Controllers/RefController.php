<?php

namespace App\Http\Controllers;

use App\Models\Ref;
use Illuminate\Http\Request;

class RefController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req){
        $penerangan = $req->penerangan;
        
        
        $mref = Ref::where(
            function($query) use ($penerangan){
                if (!empty($penerangan)){
                    $query->where('penerangan','like','%'.$penerangan.'%');
                    $query->orWhere('kod',$penerangan);
                }
            }
        )
        ->paginate(6);
        // ->get();
        return view('ref.senarai_lookup',compact('mref','penerangan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mref = new Ref();
        
        return view('ref.borang_lookup',compact('mref'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req){
        $mref = new Ref();
        $mref->kod = $req->kod;
        $mref->penerangan = $req->penerangan;
        $mref->save();
        return redirect()->route('ref.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ref  $ref
     * @return \Illuminate\Http\Response
     */
    public function show(Ref $ref)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ref  $ref
     * @return \Illuminate\Http\Response
     */
    public function edit(Ref $ref)
    {
        $mref = $ref;
        
        return view('ref.borang_lookup',compact('mref'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ref  $ref
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Ref $ref)
    {
        $mref = $ref;
        $mref->kod = $req->kod;
        $mref->penerangan = $req->penerangan;
        $mref->save();
        return redirect()->route('ref.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ref  $ref
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ref $ref)
    {
        $ref->delete($ref->id);
        return redirect()->route('ref.index');
    }
}
